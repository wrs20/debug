

#include <omp.h>
#include <stdio.h>



extern "C"
int test(){

    #pragma omp parallel for default(none)
    for(int ix=0 ; ix<2 ; ix++){
        printf("hello from test %d\n", omp_get_thread_num());
    }
    return 0;
}


extern "C"
int test2(const int N){
    #pragma omp parallel for default(none)
    for(int ix=0 ; ix<N ; ix++){
        printf("hello from test2 %d\n", omp_get_thread_num());
    }
    return 0;
}


#ifdef MAIN

int main(int argc, char* argv[]){
    
    test();
    test2(4);

    return 0;
}


#endif





