

cray:
	CC lib.cpp -DMAIN
	CC lib.cpp -shared -fPIC -o lib.so
	cp lib.so lib2.so

clang:
	armclang++ lib.cpp -DMAIN -fopenmp
	armclang++ lib.cpp -shared -fPIC -o lib.so -fopenmp
	cp lib.so lib2.so

gcc:
	g++ lib.cpp -DMAIN -fopenmp
	g++ lib.cpp -shared -fPIC -o lib.so -fopenmp
	cp lib.so lib2.so

clean:
	rm a.out lib.so lib2.so


test:
	python test_lib.py
